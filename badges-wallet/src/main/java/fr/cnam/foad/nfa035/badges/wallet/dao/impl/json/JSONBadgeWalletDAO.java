package fr.cnam.foad.nfa035.badges.wallet.dao.impl.json;


import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.OutputStream;
import java.util.SortedMap;

/**
 * Interface définissant le comportement d'un DAO destinée à la gestion de badges digitaux
 * PAR ACCES DIRECT, et en JSON
 */

public interface JSONBadgeWalletDAO extends DirectAccessBadgeWalletDAO {

    /**
     * Facilité pour obtenir un badge à partir de ses métadonnées
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException;

    /**
     * Facilité pour obtenir une Map de métadonnées
     * @return
     * @throws IOException
     */
    SortedMap<DigitalBadge, DigitalBadgeMetadata> getWalletMetadataMap() throws IOException;
}





