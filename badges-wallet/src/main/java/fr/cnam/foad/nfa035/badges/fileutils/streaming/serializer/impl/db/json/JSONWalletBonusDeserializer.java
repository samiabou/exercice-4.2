package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;

/**
 * Interface définissant le comportement attendu d'un objet servant à
 * la désérialisation d'un badge du wallet au format JSON.
 */


public interface JSONWalletBonusDeserializer extends DirectAccessDatabaseDeserializer {

    /**
     * Permet de désérialiser à partir des métadonnées
     * @param media
     * @param metas
     * @throws IOException
     */
    DigitalBadge deserialize(WalletFrameMedia media, DigitalBadgeMetadata metas) throws IOException;

    /**
     * Permet de désérialiser à partir de la position du badge dans le fichier csv
     * @param media
     * @param pos
     * @throws IOException
     */

    DigitalBadge deserialize(WalletFrameMedia media, long pos) throws IOException;
}